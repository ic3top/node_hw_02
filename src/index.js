require('dotenv').config();
const express = require('express');
const morgan = require('morgan');
const mongoose = require('mongoose');

const app = express();
const PORT = process.env.PORT;

const {authRouter} = require('./controllers/authController');
const {userRouter} = require('./controllers/userController');
const {notesRouter} = require('./controllers/notesController');
const {authMiddleware} = require('./middlewares/authMiddleware');

app.use(express.json());
app.use(morgan('tiny'));

app.use('/api/auth', authRouter);

app.use('/api/users', [authMiddleware], userRouter);
app.use('/api/notes', [authMiddleware], notesRouter);

app.use((_, res) => {
  res.status(404).json({message: 'Not found'});
});

const start = async () => {
  try {
    await mongoose.connect(process.env.MONGO_URI, {
      useUnifiedTopology: true,
      useNewUrlParser: true,
      useCreateIndex: true,
    });

    app.listen(PORT);
    console.log('...Connected!');
  } catch (err) {
    console.error(`Error on server startup: ${err.message}`);
  }
};

start();
