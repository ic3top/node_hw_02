const express = require('express');
const {
  getUserInfoByUsername,
  deleteUser,
  updatePasswordByUsername,
} = require('../services/userService');

const router = express.Router();

router.get('/me', async (req, res) => {
  try {
    const {
      username,
    } = req.user;

    const user = await getUserInfoByUsername(username);
    if (!user) {
      res.status(400).json({message: 'The user was not found'});
    }

    res.json({user});
  } catch (err) {
    res.status(500).json({message: err.message});
  }
});

router.delete('/me', async (req, res) => {
  try {
    const {
      username,
    } = req.user;

    const user = await deleteUser(username);
    if (!user) {
      return res.status(400).json(
          {message: 'The user has already been deleted'},
      );
    }

    res.json({message: 'Success'});
  } catch (err) {
    res.status(500).json({message: err.message});
  }
});

router.patch('/me', async (req, res) => {
  try {
    const {
      oldPassword,
      newPassword,
    } = req.body;

    const {
      username,
    } = req.user;

    const user = await updatePasswordByUsername(
        {
          username,
          newPassword,
          oldPassword,
        },
    );

    if (!user) {
      return res.status(400).json({message: 'Invalid input data'});
    }

    res.json({message: 'Success'});
  } catch (err) {
    res.status(500).json({message: err.message});
  }
});

module.exports = {userRouter: router};
