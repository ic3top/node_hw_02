const express = require('express');

const router = express.Router();

const {
  addNoteToUser,
  getNotesByUserId,
  getUserNoteById,
  updateUserNoteById,
  toggleUserNoteComplete,
  deleteUserNoteById,
} = require('../services/notesService');

router.get('/', async (req, res) => {
  try {
    const {offset = 0, limit = 10} = req.query;
    const {userId} = req.user;
    const {count, notes} = await getNotesByUserId({userId, offset, limit});

    res.json({offset, limit, count, notes});
  } catch (err) {
    res.status(500).json({message: err.message});
  }
});

router.post('/', async (req, res) => {
  try {
    const {userId} = req.user;

    await addNoteToUser(userId, req.body);

    res.json({message: 'Success'});
  } catch (err) {
    res.status(500).json({message: err.message});
  }
});

router.get('/:id', async (req, res) => {
  try {
    const {userId} = req.user;
    const noteId = req.params.id;
    const note = await getUserNoteById(noteId, userId);

    if (!note) {
      return res.status(400).json({message: 'You don\'t have such note :('});
    }

    res.json({note});
  } catch (err) {
    res.status(500).json({message: err.message});
  }
});

router.put('/:id', async (req, res) => {
  try {
    const {text} = req.body;

    if (!text) {
      return res
          .status(400)
          .json({message: 'Please, specify \'text\' parameter'});
    }

    const {userId} = req.user;
    const noteId = req.params.id;

    await updateUserNoteById({noteId, userId, text});

    res.json({message: 'Success'});
  } catch (err) {
    res.status(500).json({message: err.message});
  }
});

router.patch('/:id', async (req, res) => {
  try {
    const {userId} = req.user;
    const noteId = req.params.id;

    await toggleUserNoteComplete(noteId, userId);

    res.json({message: 'Success'});
  } catch (err) {
    res.status(500).json({message: err.message});
  }
});

router.delete('/:id', async (req, res) => {
  try {
    const {userId} = req.user;
    const noteId = req.params.id;

    await deleteUserNoteById(noteId, userId);

    res.json({message: 'Success'});
  } catch (err) {
    res.status(500).json({message: err.message});
  }
});

module.exports = {notesRouter: router};
