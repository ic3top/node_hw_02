const Joi = require('joi');

const registrationValidator = async (req, _, next) => {
  const schema = Joi.object({
    username: Joi.string()
        .alphanum()
        .min(3)
        .max(15)
        .required(),
    password: Joi.string()
        .min(8)
        .max(20)
        .required(),
  });

  try {
    await schema.validateAsync(req.body);
    next();
  } catch (err) {
    next(err);
  }
};

module.exports = {
  registrationValidator,
};
