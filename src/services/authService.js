const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
require('dotenv').config();

const {User} = require('../models/userModel');

const register = async ({username, password}) => {
  const user = new User({
    username,
    password: await bcrypt.hash(password, 10),
  });
  await user.save();
};

const login = async ({username, password}) => {
  const user = await User.findOne({username});

  if (!user) {
    return null;
  }

  if (!(await bcrypt.compare(password, user.password))) {
    return null;
  }

  const token = jwt.sign({
    _id: user._id,
    username: user.username,
  }, process.env.SECRET, {expiresIn: '1h'});
  return token;
};

module.exports = {
  register,
  login,
};
