const {Note} = require('../models/noteModel');

const addNoteToUser = async (userId, notePayload) => {
  const note = new Note({userId, ...notePayload});
  await note.save();
};

const getNotesByUserId = async ({userId, limit, offset}) => {
  const count = await Note.count();
  const notes = await Note
      .find({userId}, {__v: false})
      .skip(Number(offset))
      .limit(Number(limit));

  return {
    count,
    notes,
  };
};

const getUserNoteById = async (noteId, userId) => {
  const note = await Note.findOne({_id: noteId, userId}, {__v: false});
  return note;
};

const updateUserNoteById = async ({noteId, userId, text}) => {
  const unUpdatedNote = await Note.findOneAndUpdate({
    _id: noteId,
    userId,
  }, {text});
  return unUpdatedNote;
};

const toggleUserNoteComplete = async (noteId, userId) => {
  const note = await Note.findOne({_id: noteId, userId});
  await Note.findOneAndUpdate(
      {_id: noteId, userId},
      {completed: !note.completed},
  );

  return note;
};

const deleteUserNoteById = async (noteId, userId) => {
  const deletedNote = await Note.findOneAndDelete({_id: noteId, userId});
  return deletedNote;
};

module.exports = {
  addNoteToUser,
  getNotesByUserId,
  getUserNoteById,
  updateUserNoteById,
  toggleUserNoteComplete,
  deleteUserNoteById,
};
