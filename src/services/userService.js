const {User} = require('../models/userModel');
const bcrypt = require('bcrypt');

const getUserInfoByUsername = async (username) => {
  const user = await User.findOne({username}, '_d username createdDate');
  return user;
};

const deleteUser = async (username) => {
  const deletedUser = await User.findOneAndDelete({username});
  return deletedUser;
};

const updatePasswordByUsername = async ({
  username,
  newPassword,
  oldPassword,
}) => {
  const user = await User.findOne({username}, 'password');

  if (!(await bcrypt.compare(oldPassword, user.password))) {
    throw new Error('Invalid password');
  }

  const updatedUser = await User.findOneAndUpdate(
      {username},
      {password: await bcrypt.hash(newPassword, 10)},
  );

  return updatedUser;
};

module.exports = {getUserInfoByUsername, deleteUser, updatePasswordByUsername};
